package puzzle_busqueda_anchura;

import java.util.Random;
import java.util.Vector;

public class App {

	private static final String ESTADO_FINAL = "123456780";
	
	public App() {
		
		this.iniciar();
	}
	
	private void iniciar() {
		
		String estadoInicial = generarEstadoInicial();
		
		OperacionesPuzzle opereacionesPuzzle = new OperacionesPuzzle();
		Nodo nodo = new Nodo(estadoInicial, new Vector <Nodo> (), 0);
		
		BusquedaAnchura busquedaAnchura = new BusquedaAnchura(opereacionesPuzzle, this.ESTADO_FINAL);
		busquedaAnchura.buscarPorAnchura(nodo);
	}
	
	private String generarEstadoInicial() {
	
		int numero = 0;
		char caracter;
		String cadena = "";
		Random random = new Random();
		
		for(int i = 0; i < ESTADO_FINAL.length() ; ) {
			
			numero = random.nextInt(ESTADO_FINAL.length());
			caracter = ESTADO_FINAL.charAt(numero);
			
			if(cadena.indexOf(caracter) == -1) {
				
				cadena = cadena + caracter;
				i++;
			}
		}
		
		return cadena;
	}
	
	public static void main (String [] args) {
		
		new App();
	}
	
}
