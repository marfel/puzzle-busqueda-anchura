package puzzle_busqueda_anchura;

import java.util.Vector;

public class BusquedaAnchura {
	
	private int numEstados;
	private OperacionesPuzzle operacionesPuzzle;
	private String estadoFinal;
	
	public BusquedaAnchura(OperacionesPuzzle operacionesPuzzle, String estadoFinal) {
		
		this.operacionesPuzzle = operacionesPuzzle;
		this.estadoFinal = estadoFinal;
	}
	
	public void buscarPorAnchura(Nodo nodo) {
		
		if(nodo.getPuzzle().equals(estadoFinal)) {
			
			System.out.println("Estado Inicial = Estado final");
			return;
		}
			
		operacionesPuzzle.buscarMovimientos(nodo, estadoFinal);
		
		Vector <Nodo> nodos = nodo.getNodes();
		
		int numNiveles = 0;
		
		System.out.println("-------------------- Nivel = " + numNiveles +  " --------------------");
		System.out.println("Estado Inicial: ");
		operacionesPuzzle.imprimirPuzzle(nodo.getPuzzle());
		
		while(true) {
			
			numNiveles ++;
			System.out.println("-------------------- Nivel = " + numNiveles +  " --------------------");
			
			if(buscar(nodos)) {
				
				System.out.println("-------------------- Resumen --------------------");
				System.out.println();
				System.out.println("# de niveles generados: " + numNiveles);
				System.out.println("# de estados generados: " + this.numEstados);
				
				return;
		
			} else {
				
				nodos = generarNivel(nodos);
			}
		}
	}
	
	private Vector <Nodo> generarNivel(Vector <Nodo> nodos) {
		
		Vector <Nodo> nivel = new Vector <Nodo> ();
		
		for(int i = 0 ; i < nodos.size() ; i ++) {
			
			Nodo nodo = nodos.get(i);
			operacionesPuzzle.buscarMovimientos(nodo, estadoFinal);
			nivel.addAll(nodo.getNodes());
		}
		
		return nivel;
	}
	
	private boolean buscar(Vector <Nodo> nodos) {
		
		boolean encontro = false;
		
		for(int i = 0 ; i < nodos.size() ; i ++ ) {
			
			this.numEstados ++;
			
			System.out.println("Estado:");
			operacionesPuzzle.imprimirPuzzle(nodos.get(i).getPuzzle());
			
			if(nodos.get(i).getPuzzle().equals(estadoFinal)) {
				
				encontro = true;
				System.out.println("Estado Final: ");
				operacionesPuzzle.imprimirPuzzle(nodos.get(i).getPuzzle());
				break;
			} 
		}
		
		return encontro;
	}
	
}
