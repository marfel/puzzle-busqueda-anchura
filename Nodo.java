package puzzle_busqueda_anchura;

import java.util.Vector;

public class Nodo {
	
	private String puzzle;
	private Vector <Nodo> nodos;
	private int restrictMove;
	
	public Nodo(String puzzle, Vector <Nodo> nodos, int restrictMove) {
		
		this.puzzle = puzzle;
		this.nodos = nodos;
		this.restrictMove = restrictMove;
	}
	
	public String getPuzzle() {
		
		return puzzle;
	}
	
	public Vector <Nodo> getNodes() {
		
		return nodos;
	}

	public int getRestrictMove() {
		
		return restrictMove;
	}

}
