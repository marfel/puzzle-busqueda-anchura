package puzzle_busqueda_anchura;

import java.util.Hashtable;
import java.util.Vector;

public class OperacionesPuzzle {

	private Hashtable <String, String> hashTable;
	
	public OperacionesPuzzle() {
		
		hashTable = new Hashtable <String, String> ();
	}
	
	private String mover(String puzzle, int posicionVacia, int posicionAcambiar) {
		
		char numeroAcambiar = puzzle.charAt(posicionAcambiar);
		
		StringBuilder nuevoPuzzle = new StringBuilder(puzzle);
		
		nuevoPuzzle.setCharAt(posicionVacia, numeroAcambiar);
		nuevoPuzzle.setCharAt(posicionAcambiar, '0');
		
		return nuevoPuzzle.toString();
	}
	
	private String moverArriba(String puzzle) {
		
		String nuevoPuzzle = "";
		int posicionVacia = puzzle.indexOf('0');
		int posicionAcambiar = posicionVacia - 3;
		
		if(posicionAcambiar >= 0)
			nuevoPuzzle = mover(puzzle, posicionVacia, posicionAcambiar);
		
		return nuevoPuzzle;
	}
	
	private String moverAbajo(String puzzle) {
		
		String nuevoPuzzle = "";
		int posicionVacia = puzzle.indexOf('0');
		int posicionAcambiar = posicionVacia + 3;
		
		if(posicionAcambiar < puzzle.length())
			nuevoPuzzle = mover(puzzle, posicionVacia, posicionAcambiar);
		
		return nuevoPuzzle;
	}	
	
	private String moverDerecha(String puzzle) {
		
		String nuevoPuzzle = "";
		int posicionVacia = puzzle.indexOf('0');
		int posicionAcambiar = posicionVacia + 1;
		
		if(posicionAcambiar < puzzle.length() && posicionVacia != 2 && posicionVacia != 5 && posicionVacia != 8)
			nuevoPuzzle = mover(puzzle, posicionVacia, posicionAcambiar);
		
		return nuevoPuzzle.toString();
	}
	
	private String moverIzquierda(String puzzle) {
		
		String nuevoPuzzle = "";
		int posicionVacia = puzzle.indexOf('0');
		int posicionAcambiar = posicionVacia - 1;
		
		if(posicionAcambiar >= 0 && posicionVacia != 0 && posicionVacia != 3 && posicionVacia != 6)
			nuevoPuzzle = mover(puzzle, posicionVacia, posicionAcambiar);
		
		return nuevoPuzzle;
	}
	
	public void buscarMovimientos(Nodo nodo, String estadoFinal) {
		
		if(nodo.getPuzzle().equals(estadoFinal))
			return;
		
		int posicionVacia = nodo.getPuzzle().indexOf('0');
		int restrictCode = nodo.getRestrictMove();
		String nuevoPuzzle = "";
		
		switch(posicionVacia) {
		
			case 0:
				
				if(restrictCode != 3) {
					
					nuevoPuzzle = moverDerecha(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 1);
				}
				
				if(restrictCode != 4) {
					
					nuevoPuzzle = moverAbajo(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 2);
				}
				
				break;
				
			case 1:
				
				if(restrictCode != 1) {
					
					nuevoPuzzle = moverIzquierda(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 3);
				}
				
				if(restrictCode != 3) {
					
					nuevoPuzzle = moverDerecha(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 1);
				}
				
				if(restrictCode != 4) {
					nuevoPuzzle = moverAbajo(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 2);
				}
				
				break;
				
			case 2:
				
				if(restrictCode != 1) {
					
					nuevoPuzzle = moverIzquierda(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 3);
				}
				
				if(restrictCode != 4) {
					
					nuevoPuzzle = moverAbajo(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 2);
				}
				
				break;
				
			case 3:
				
				if(restrictCode != 2) {
					
					nuevoPuzzle = moverArriba(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 4);
				}
				
				if(restrictCode != 3) {
					
					nuevoPuzzle = moverDerecha(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 1);
				}
				
				if(restrictCode != 4) {
					
					nuevoPuzzle = moverAbajo(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 2);
				}
				
				break;
				
			case 4:
				
				if(restrictCode != 1) {
					
					nuevoPuzzle = moverIzquierda(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 3);
				}
				
				if(restrictCode != 2) {
					
					nuevoPuzzle = moverArriba(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 4);
				}
				
				if(restrictCode != 3) {
					
					nuevoPuzzle = moverDerecha(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 1);
				}
				
				if(restrictCode != 4) {
					
					nuevoPuzzle = moverAbajo(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 2);
				}
				
				break;
				
			case 5:
				
				if(restrictCode != 1) {
					
					nuevoPuzzle = moverIzquierda(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 3);
				}
				
				if(restrictCode != 2) {
					
					nuevoPuzzle = moverArriba(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 4);
				}
				
				if(restrictCode != 4) {
					
					nuevoPuzzle = moverAbajo(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 2);
				}
				
				break;
				
			case 6:
				
				if(restrictCode != 2) {
					
					nuevoPuzzle = moverArriba(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 4);
				}
				
				if(restrictCode != 3) {
					
					nuevoPuzzle = moverDerecha(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 1);
				}
				
				break;
				
			case 7:
				
				if(restrictCode != 1) {
					
					nuevoPuzzle = moverIzquierda(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 3);
				}
				
				if(restrictCode != 2) {
					
					nuevoPuzzle = moverArriba(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 4);
				}
				
				if(restrictCode != 3) {
					
					nuevoPuzzle = moverDerecha(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 1);
				}
				
				break;
				
			case 8:
				
				if(restrictCode != 1) {
					
					nuevoPuzzle = moverIzquierda(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 3);
				}
				
				if(restrictCode != 2) {
					
					nuevoPuzzle = moverArriba(nodo.getPuzzle());
					llaveExiste(nuevoPuzzle, nodo, 4);
				}
				
				break;
		}
	}
	
	private void llaveExiste(String llave, Nodo nodo, int restrictMov) {
		
		if(hashTable.get(llave) != null) {
			
			return;
		}
		
		hashTable.put(llave, llave);
		nodo.getNodes().add(new Nodo (llave, new Vector <Nodo> (), restrictMov));
	}
	
	public void imprimirPuzzle(String puzzle) {
		
		System.out.println(puzzle.substring(0, 3));
		System.out.println(puzzle.substring(3, 6));
		System.out.println(puzzle.substring(6, 9));
		System.out.println("");
	}
	
}
